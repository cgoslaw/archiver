# Archiver Script
Script that can be used to either backup or archive remote backed up files. It can be easily modified to whatever purposes you might find.

All sensitive data has been scrubbed.

**Please read the instructions & change the variables as you see fit** 

*Requires: rsync & mailx*

## For those that enjoy a sysadmin story
When I joined my previous company, I was given the role of responsible person for all the Linux servers. Being a company that had most of its infrastructure based on Microsoft, they never had any Linux sysadmin and thus most of the server were really in a bad shape.

One of these servers was an old Wheezy Debian that seemed to have been upgraded to Jessie once and left forgotten ever since. My first task was to fix its sshd service that, as my colleague said, have been some months out of work and no one could fix it and it was a very important server. That specifically got my attention and I was really interested on it. It was a simple "Match User" misconfiguration and after fixing I was "rewarded" the server so I could take care of it.

This server ran many scripts, most of them being backup scripts for clients. One day I was given the task to update it. I have to admit and say that I'm not really a Debian guy. I never was really fond of the "Debian way". Updating it to where it was supposed to be would take a lot of my time and it wasn't certain it would work at all after upgrading it.
So I brought up the idea of migrating the services to an EL and that was very well received.

When I started testing the scripts, one of them - the archiver, just didn't run. It wasn't well written, but there was also integration with a php software to send emails. And so I set upon myself the task of rewriting all the scripts.

Sadly, I wasn't able to finish all of them but this one. It isn't something really new or a masterpiece, but I really enjoyed how this one turned out.

