#!/bin/bash
# Enable Verbose & Debugging
# set -v on
# set -x on

## Notes ###############################################################################################
## [Author]             Cesar Goslawski (cgoslaw)
## [Description]        Script intended for archiving and storaging the clients' backed up .conf files.
##			
##			This script uses globbing to expand into arrays of directories, 
##			separating each backup file between clients. This ensures that there's no need
##			for changes to the code if adding a new client to the backup list.
##
## [Version]            v1.1 (06/06/2021)
## [Email]              c.goslawski@ya.ru
########################################################################################################

# ======================================================================
# Directory Variables --------------------------------------------------
# ======================================================================
# [1] Source Directory - Where the files are primarily created
# upon execution of the backup script.
# [2] Temporary Directory - Where files are copied to, in order to test
# the script, if needed, without messing with the original files.
# [3] Destiny Directory - Where the tar files are going to be sent to.
# The directories are separated by Client names.
SRC_DIR=(/srv/data/dir/*)
TMP_DIR=(/tmp/data/dir)
DST_DIR=(/srv/backup/dir)

# ======================================================================
# Data Variables -------------------------------------------------------
# ======================================================================
# [1] Glob in order to expand into the clients directory during the
# backup process;
# [2] Log File
TAR_FILES=(`basename -a /srv/data/dir/*`)
LOG_FILE=(/var/log/archiver.log)

# =======================================================================
# Email Variables -------------------------------------------------------
# =======================================================================
MAIL_RECIPIENTS="example@example.net"
MAIL_SENDER="Backup <backup@example.net>"
MAIL_SUBJECT="Backup Result - `date +%d/%m/%Y`"
MAIL_FILE=(/opt/script/backup/dir/mail.letter)
MAIL_RESULT=(/opt/script/backup/dir/results.log)

# =======================================================================
# Counter Variables -----------------------------------------------------
# =======================================================================
# [1] Counter used to count all dirs inside the $SRC_DIR;
# [2] Counter used to be counted against the $ELEMCOUNT in case other
# dirs have to be created afterwards;
# [3] Counter used to count against $ELEMCOUNT during the archiving;
# [4] Counter used to count all the locations and jobs performed;
# [5] Counter used to count the successful backup jobs;
# [6] Counter used to count the failed backup jobs.
ELEMCOUNT="${#SRC_DIR[@]}"
COUNTER=0;
SYNCOUNT=0;
TOTAL_COUNTER=0;
SUCCESS_COUNTER=0;
FAILURE_COUNTER=0;

while [ $COUNTER -lt $ELEMCOUNT ]
do
	SAVE_FROM[$COUNTER]=${SRC_DIR[$COUNTER]}
	if [ ${#TMP_DIR[@]} -eq 1 ]; then
		SAVE_TO[$COUNTER]=${TMP_DIR[0]}
	else
		:
	fi

	let COUNTER=$COUNTER+1
done

# ====================================================================== #
# ------------------          Backup Process          ------------------ #
# ====================================================================== #
# Makes sure $TMP_DIR exists so it can be used to hold the temporary files
if [[ ! -d $TMP_DIR ]]; then
        /usr/bin/mkdir -p $TMP_DIR && chown backup: $TMP_DIR
else
        :
fi

# Creates the Client tar directories
for dir in ${TAR_FILES[@]}; do
	if [[ ! -d $DST_DIR/${TAR_FILES[@]} ]]; then
		mkdir -p $DST_DIR/$dir
	else
		:
	fi
done

# Sync all the files from a source directory to the temporary dir,
# then the data is tarred into a tarball and deleted thereafter.
# Note: Add or remove the option --remove-source-files in rsync as to
# either remove the files after the backup or keep them for testing.
while [ $SYNCOUNT -lt $ELEMCOUNT ];
do
        /usr/bin/rsync --remove-source-files -a -E -h -v --no-relative ${SAVE_FROM[$SYNCOUNT]} ${SAVE_TO[$SYNCOUNT]}
	/usr/bin/tar -pcjf $DST_DIR/${TAR_FILES[$SYNCOUNT]}/${TAR_FILES[$SYNCOUNT]}_BKP-`date +%F`.tbz2 -C ${TMP_DIR}/${TAR_FILES[$SYNCOUNT]} .
        let SYNCOUNT=$SYNCOUNT+1;
done

# =======================================================================
# Verification & Clean-up Process ---------------------------------------
# =======================================================================
# Backup verification process, passing a status of success or failure,
# depending on whether the script can find a .conf file inside the
# directory used for the check-up, currently the same as the Tmp Dir.
TMP_DIR_CUS=($TMP_DIR/*)

printf "\t--------------------\n\t-- Backup Results --\n\t--------------------\n\n" > $MAIL_RESULT
for ((c=0; c<${#TMP_DIR_CUS[@]}; c++)); do
        location=$(find ${TMP_DIR_CUS[$c]}/* -type d -name "*" )
	printf "==========================================\n" >> $MAIL_RESULT
	printf "\t-- %s - %s --\n" "Client"  "$(basename -a ${TMP_DIR_CUS[$c]})" >> $MAIL_RESULT
        for l in $location; do
		let TOTAL_COUNTER=$TOTAL_COUNTER+1;
                if [[ `compgen -G $l/*.conf` ]]; then
			printf "Location = %-6s	[Success]\n" "$(basename $l)" >> $MAIL_RESULT && let SUCCESS_COUNTER=$SUCCESS_COUNTER+1;
                else
			printf "Location = %-6s	[Failure]\n" "$(basename $l)" >> $MAIL_RESULT && let FAILURE_COUNTER=$FAILURE_COUNTER+1;
                fi
        done
done
printf "==========================================\n" >> $MAIL_RESULT

# Cleaning up the temporary files
/usr/bin/rm -rf ${TMP_DIR[@]}

# Cleaning up the source files in case of not removing them during the backup process with rsync
# /usr/bin/rm -rf ${SRC_DIR[@]}

# =======================================================================
# Mail Preparation & Sending --------------------------------------------
# =======================================================================
# Mail preparation process to send the results to the distribution list.
printf "Dear All,\n\nBelow are the results for the configuration backup job of today, %(%D)T.\n" > $MAIL_FILE
printf "\nFrom a grand total of $TOTAL_COUNTER locations and devices, $SUCCESS_COUNTER backups were successfully executed while $FAILURE_COUNTER resulted in failure.\n" >> $MAIL_FILE
printf "Further details of today's backup job can be found in the attached log file.\n" >> $MAIL_FILE
printf "\nIn case of questions or concerns, please contact the responsible team\n" >> $MAIL_FILE
printf "\nKind regards,\nBackup, the Automated Backup Daemon" >> $MAIL_FILE

# Sending the e-mail with the variables assigned.
/usr/bin/mailx -a $MAIL_RESULT -s "$MAIL_SUBJECT" -r "$MAIL_SENDER" "$MAIL_RECIPIENTS" < $MAIL_FILE

exit 0
